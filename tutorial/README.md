# MetaVal Tutorial

This tutorial demonstrates how MetaVal can be used for witness-based result validation.
For executing the examples shown, the virtual machine from the Zenodo archive can be used
[[DOI: 10.5281/zenodo.3630165](https://doi.org/10.5281/zenodo.3630165)].

**Note:** The purpose of the artifact is not to fully reproduce all validation results by MetaVal of
the competition on software verification (SV-COMP 2020),
since this is already ensured via the artifact of the [competition report](https://doi.org/10.1007/978-3-030-45237-7_21).
Instead, we showcase how MetaVal can be used
by taking selected examples for validation from each category of the competition.

The virtual machine should ideally have 2 processing units and about 10 GB of RAM
(less RAM might also work, but the tool was run with 7 GB of RAM in SV-COMP 2020)

The following commands were executed to create the environment and most of files in the artifact:

    git clone --branch svcomp20 --depth=1 https://github.com/sosy-lab/sv-benchmarks.git
    # we also removed larger files in this clone of the sv-benchmarks repository to keep the artifact size manageable
    sudo apt install wget openjdk-8-jre openjdk-11-jre python python-tempita lxcfs
    mkdir cav20 && cd cav20
    # latest version from gitlab CI job (go to https://gitlab.com/sosy-lab/software/metaval/-/jobs/ for actual link):
    wget "https://gitlab.com/sosy-lab/software/metaval/-/jobs/<jobid>/artifacts/file/metaval.zip" -O metaval.zip
    # alternatively, use version from SV-COMP 2020:
    # wget "https://gitlab.com/sosy-lab/sv-comp/archives-2020/raw/master/2020/val_metaval.zip?inline=false" -O metaval.zip
    mkdir cav20 && cd cav20
    unzip ../metaval.zip

# Checking Whether MetaVal Works

The scenario is that we want to reproduce some of the validation results of MetaVal from SV-COMP 2020.
This section will describe how to proceed to retrieve the necessary information.
The information and files are alternatively already present in the `tutorial/` folder,
so no internet connection is required to run the tool.
Below we will assume that the current working directory is `~/cav20/metaval` if not stated otherwise.

## A Quick Check via BenchExec

For a quick check, we can execute BenchExec on some example witnesses. For correctness witnesses, we can use the following:

    benchexec tutorial/tests_correctness.xml

This should produce an output similar to the following:

    executing run set 'correctness'     (3 files)
    12:52:02   array-examples/sanfoundry_43_ground.yml         true                     98.47   91.14
    12:53:33   memsafety-ext3/getNumbers3.yml                  true                      4.65    2.90
    12:53:36   termination-crafted/TelAviv-Amir-Minimum.yml    true                     13.61    7.87
    
    Statistics:              3 Files
      correct:               3
        correct true:        3
        correct false:       0
      incorrect:             0
        incorrect true:      0
        incorrect false:     0
      unknown:               0
      Score:                 6 (max: 6)
    
    In order to get HTML and CSV tables, run
    table-generator 'results/tests_correctness.2020-05-16_0252.results.correctness.xml.bz2'

The "true" values in the third column indicate that we successfully confirmed three witnesses.
For violation witnesses, we can run:

    benchexec tutorial/tests_violation.xml

This should produce an output like the following:

    executing run set 'violation'     (4 files)
    12:57:37   array-tiling/skippedu.yml             false(unreach-call)      10.64    5.78
    12:57:43   memsafety-ext3/derefAfterFree1.yml    false(valid-deref)        4.47    2.71
    12:57:46   termination-crafted/2Nested-2.yml     false(no-overflow)       13.29    7.92
    12:57:56   bitvector/jain_1-1.yml                false(termination)       10.97    6.31
    
    Statistics:              4 Files
      correct:               4
        correct true:        0
        correct false:       4
      incorrect:             0
        incorrect true:      0
        incorrect false:     0
      unknown:               0
      Score:                 4 (max: 4)
    
    In order to get HTML and CSV tables, run
    table-generator 'results/tests_violation.2020-05-16_0257.results.violation.xml.bz2'

We used two different XML files because the resource limits in SV-COMP are different for 
validation using violation (timelimit 90 s)
and correctness (900 s) witnesses. For a detailed documentation of BenchExec and how to specify
benchmark definitions, please have a look at the [BenchExec documentation](https://github.com/sosy-lab/benchexec/tree/master/doc).

## Executing MetaVal Directly

Of course MetaVal can also be executed directly without BenchExec. In this section, we will have a look
how to replicate individual validation results from SV-COMP 2020.
For getting the necessary information for replication we have to visit https://sv-comp.sosy-lab.org/2020/results/results-verified/

We look at the desired table of the subcategory where we want to check the validation, e.g., ReachSafety-Arrays for CPA-Seq:
![image of how to find the right table](screenshots/navigation_svcomp_site.png)

The table can also be directly accessed via this link:
https://sv-comp.sosy-lab.org/2020/results/results-verified/cpa-seq.2019-11-29_1400.results.sv-comp20_prop-reachsafety.ReachSafety-Arrays.xml.bz2.merged.xml.bz2.table.html

Let's say we want to replicate the correctness validation of MetaVal for the task array-examples/sanfoundry_43_ground.yml
We locate the row of the task in the table. The verifier results are in the first 8 columns after the task name.
One of them contains a link to the witness with text "view":
![image of how to find the "view" link in the table](screenshots/navigation_svcomp_site_table.png)

The link should look like this: 
https://sv-comp.sosy-lab.org/2020/results/fileByHash/5e3cbba86387b2cd5d0a9a1a6a5e4c8e83d6979d30837f172827f2b250368643.graphml

we can download this witness:

wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/5e3cbba86387b2cd5d0a9a1a6a5e4c8e83d6979d30837f172827f2b250368643.graphml" -O tutorial/witnesses/sanfoundry_43_ground.graphml

Now we need to find out with which parameter MetaVal was called. For that we scroll to the column block that contains metaval-validate-correctness-witnesses in the title.
After clicking on the validation verdict "true" that MetaVal returned, we see the command line that was called followed by the tool output.
The command line is as follows:

./metaval.sh --verifier 'CPAchecker-1.7-svn 29852-unix' --witness ../../results-verified/cpa-seq.2019-11-29_1400.files/sv-comp20_prop-reachsafety.sanfoundry_43_ground.yml/witness.graphml ../../sv-benchmarks/c/array-examples/sanfoundry_43_ground.i -- scripts/cpa.sh -svcomp19 -heap 10000M -benchmark -timelimit 900s -stats -spec ../../../sv-benchmarks/c/properties/unreach-call.prp ../output/ARG.c

Here we need to adapt the path to the witness and usually also the path to the verified program (`sanfoundry_43_ground.i`), and the path to the specification file (unreach-call.prp).
In the VM, however, the sv-benchmarks repository is already at the right place, so the paths to the program and the specification file should already be correct.
Note that some of the bigger files in the sv-benchmarks repository were removed in the artifact in order to keep the artifact size moderate.
These can always be retrieved from https://github.com/sosy-lab/sv-benchmarks/tree/svcomp20 if desired.

The parameters behind the "--" parameter are the parameters that are passed to the backend verifier once the program carrying the witness semantics has been generated using the options before the "--" parameter

So we execute the following command from within the metaval folder:

    ./metaval.sh --verifier 'CPAchecker-1.7-svn 29852-unix' --witness sanfoundry_43_ground.graphml ../../sv-benchmarks/c/array-examples/sanfoundry_43_ground.i -- scripts/cpa.sh -svcomp19 -heap 10000M -benchmark -timelimit 900s -stats -spec ../../../sv-benchmarks/c/properties/unreach-call.prp ../output/ARG.c

which will first generate the program (which can be found in output/ARG.c) and then run the verifier specified with the --verifier option.
The tool terminates after around 90 seconds with the following output:

    Verification result: TRUE. No property violation found by chosen configuration.
    More details about the verification run can be found in the directory "./output".

So we reproduced the confirmed validation result for the task `sanfoundry_43_ground.yml` for the verifier CPA-Seq.

In order to demonstrate that our tool works for each of the categories mentioned in the paper,
we prepared the validator calls for one example for correctness/violation witness validation in each category.
The witnesses have already been downloaded, so there is no need to perform this step.
Versions of these validation commands that work locally without requiring an internet connection can
be found in the script `tutorial/smoke_tests.sh`. This script is executed by MetaVal's CI in order to
ensure that MetaVal can always be executed with the supported verifiers.
We can of course manually run that script, which will sequentially run the validation of the 7 validation tasks we
executed earlier via BenchExec:

    tutorial/smoke_test.sh

If the exit code of this script is 0, this means that all executions were successful.

### Confirmation of Correctness Witness for ReachSafety-Arrays for CPA-Seq:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/5e3cbba86387b2cd5d0a9a1a6a5e4c8e83d6979d30837f172827f2b250368643.graphml" -O tutorial/witnesses/sanfoundry_43_ground.yml.graphml

Validator call:

    ./metaval.sh --verifier 'CPAchecker-1.7-svn 29852-unix' --witness tutorial/witnesses/sanfoundry_43_ground.yml.graphml ../../sv-benchmarks/c/array-examples/sanfoundry_43_ground.i -- scripts/cpa.sh -svcomp19 -heap 10000M -benchmark -timelimit 900s -stats -spec ../../../sv-benchmarks/c/properties/unreach-call.prp ../output/ARG.c

### Confirmation of Violation Witness for ReachSafety-Arrays for ESBMC:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/e3ae9a094891667f241bd84bcd280e3a82125d24399ddd8d1f7ae08e055a5085.graphml" -O tutorial/witnesses/skippedu.yml.graphml

Validator call:

    ./metaval.sh --verifier 'CPAchecker-1.7-svn 29852-unix' --witness tutorial/witnesses/skippedu.yml.graphml ../../sv-benchmarks/c/array-tiling/skippedu.c -- scripts/cpa.sh -svcomp19 -heap 10000M -benchmark -timelimit 90s -stats -spec ../../../sv-benchmarks/c/properties/unreach-call.prp ../output/ARG.c

### Confirmation of Correctness Witness for MemSafety-Other for UAutomizer:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/498f69672e119100db5c3ac0327d6f6948754e4bcdba7fa3e7a894105f42bb05.graphml" -O witnesses/getNumbers3.yml.graphml

Validator call:

    ./metaval.sh --verifier symbiotic --witness tutorial/witnesses/getNumbers3.yml.graphml ../../sv-benchmarks/c/memsafety-ext3/getNumbers3.c -- ./bin/symbiotic --32 --prp=../../../sv-benchmarks/c/properties/valid-memsafety.prp ../output/ARG.c

### Confirmation of Violation Witness for MemSafety-Other for UAutomizer:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/15326567727b64c0bc7ff3f65d4e044ada090fcdff9f60c7859eaed89d7e063c.graphml" -O tutorial/witnesses/derefAfterFree1.yml.graphml

Validator call:

    ./metaval.sh --verifier symbiotic --witness tutorial/witnesses/derefAfterFree1.yml.graphml ../../sv-benchmarks/c/memsafety-ext3/derefAfterFree1.c -- ./bin/symbiotic --32 --prp=../../../sv-benchmarks/c/properties/valid-memsafety.prp ../output/ARG.c

### Confirmation of Correctness Witness for NoOverflows-BitVector for CPA-Seq:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/8adf88d41ed4b7843b9acf17bc49d01e3025f6fca15673f299b1d56f8c5cd655.graphml" -O tutorial/witnesses/TelAviv-Amir-Minimum.yml.graphml

Validator call:

    ./metaval.sh --verifier UAutomizer-linux --witness tutorial/witnesses/TelAviv-Amir-Minimum.yml.graphml --additionalPATH /usr/lib/jvm/java-8-openjdk-amd64/bin ../../sv-benchmarks/c/termination-crafted/TelAviv-Amir-Minimum.c -- ./Ultimate.py --spec ../../../sv-benchmarks/c/properties/no-overflow.prp --file ../output/ARG.c --full-output --architecture 64bit

### Confirmation of Violation Witness for NoOverflows-BitVector for VeriFuzz:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/861494ab4fac0463c3cb1a53a7672d5a19a8c06f61aac3e5f2bb165e5e755788.graphml" -O tutorial/witnesses/2Nested-2.yml.graphml

Validator call:

    ./metaval.sh --verifier UAutomizer-linux --witness tutorial/witnesses/2Nested-2.yml.graphml --additionalPATH /usr/lib/jvm/java-8-openjdk-amd64/bin ../../sv-benchmarks/c/termination-crafted/2Nested-2.c -- ./Ultimate.py --spec ../../../sv-benchmarks/c/properties/no-overflow.prp --file ../output/ARG.c --full-output --architecture 64bit


### Confirmation of Violation Witness for Termination-Other for 2LS:

Witness file:

    wget "https://sv-comp.sosy-lab.org/2020/results/fileByHash/a57af63a8fb99a08145704cbb1f1f778f41c88e9a478f3b2c38652a081041cbe.graphml" -O tutorial/witnesses/jain_1-1.yml.graphml

Validator call:

    ./metaval.sh --verifier UAutomizer-linux --witness tutorial/witnesses/jain_1-1.yml.graphml --additionalPATH /usr/lib/jvm/java-8-openjdk-amd64/bin ../../sv-benchmarks/c/bitvector/jain_1-1.c -- ./Ultimate.py --spec ../../../sv-benchmarks/c/properties/termination.prp --file ../output/ARG.c --full-output --architecture 32bit
