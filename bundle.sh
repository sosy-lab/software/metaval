#!/bin/bash

# bundle metaval as metaval.zip or if a parameter is passed to this script, use that as target location:
target=${1:-$PWD/metaval.zip}

if [ -f "$target" ]
  then
  rm "$target"
fi

# temporary directory for bundling metaval, cann be passed as second parameter from command line:
buildparentdir=${2:-.}
builddir="$buildparentdir/metaval"
# check if metaval folder exists (e.g. leftover from previous bundle that failed)
if [ -d "$builddir" ]
  then
  echo "metaval folder already exists, make sure to run this script in a clean environment"
  exit 1
fi

mkdir "$builddir"

# download frontend verifier if not yet present:
if [ ! -f CPAchecker.zip ]
  then
  curl "https://zenodo.org/records/12663059/files/CPAchecker-3.0-unix.zip" --output CPAchecker.zip
fi

# download backend verifiers if not yet present:
for tool in cpachecker.zip # symbiotic.zip uautomizer.zip #yogar-cbmc.zip esbmc-kind.zip
  do
    if [ ! -f $tool ]
      then
      if [ $tool == "cpachecker.zip" ]
        then
        curl "https://zenodo.org/records/12663059/files/CPAchecker-3.0-unix.zip" --output $tool
      fi
    fi
done

basedir=$PWD
cd $builddir

# prepare CPAchecker version used in frontend:
unzip "$basedir/CPAchecker.zip"
mv CPAchecker*unix CPAchecker-frontend

# prepare verifiers used as backend:
#unzip "$basedir/yogar-cbmc.zip"
unzip "$basedir/cpachecker.zip"
mv CPAchecker*unix CPAchecker
#unzip "$basedir/esbmc-kind.zip"
#unzip "$basedir/symbiotic.zip"
#unzip "$basedir/uautomizer.zip"

cd "$basedir"

# copy the actual metaval-specific stuff
git describe --dirty > "$builddir/VERSION.txt"
cp LICENSE "$builddir/LICENSE"
cp README.md "$builddir/README.md"
cp metaval.sh "$builddir/metaval.sh"
cp metaval.py "$builddir/metaval.py"
cp -r tutorial "$builddir/tutorial"

if [ $(command -v pandoc) ]
  then
  pandoc "$builddir/tutorial/README.md" -o "$builddir/tutorial/README.html"
  pandoc "$builddir/README.md" -o "$builddir/README.html"
fi

# add a bash script that checks the commands from tutorial/README.md
derivedtests="$builddir/tutorial/smoke_tests.sh"
echo -ne "#/bin/bash\nset -e\n" > "$derivedtests"
grep --no-group-separator "Validator call" "tutorial/README.md"  -A2 | \
grep -v Validator | \
sed '/^$/d' | \
sed 's|^    ||g' | \
sed 's|../../sv-benchmarks/c|tutorial/programs|g' >> "$derivedtests"
chmod u+x "$derivedtests"

# generate the tool archive:
cd "$buildparentdir"
echo "zip -r $target metaval"
zip -r "$target" "metaval"
cd $basedir

# cleanup:
if [ -f "$builddir/metaval.sh" ]
  then
  rm -rf "$builddir"
fi
