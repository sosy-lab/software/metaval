#!/usr/bin/env python3

import argparse
import logging
import os
import pathlib
import re
import subprocess
import sys


def get_logger(name, formatter_string):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    formatter = logging.Formatter(formatter_string)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def resource(relpath):
    return pathlib.Path(__file__).parent / relpath


def version():
    with open(resource("VERSION.txt"), "r") as f:
        version = f.read().strip()
        return f"{version}"


__version__ = version()


def get_argument_parser():
    parser = argparse.ArgumentParser(
        description="Validator for Software Verification Witnesses."
    )

    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument("--verifier", type=str, required=True)
    parser.add_argument("--witness", type=str, required=True)
    parser.add_argument("--additionalPATH", type=str)
    parser.add_argument("--witnessType", type=str)
    parser.add_argument("--property", type=str, required=True)
    parser.add_argument("taskname", type=str)
    parser.add_argument(
        "wrapped_args",
        nargs="+",
        metavar="ARG",
        help='command line to run (prefix with "--" to ensure all arguments are treated correctly)',
    )
    return parser


def resource(relpath):
    return pathlib.Path(__file__).parent / relpath


def check_witness_type_matches(witness, witness_type):
    type_ = extract_type(witness)
    return witness_type == type_


def resolve_property(property_):
    property_text = open(property_, "r").read()
    if "valid-deref" in property_text:
        encoding = "REACHASMEMSAFETY"
    elif "overflow" in property_text:
        encoding = "REACHASOVERFLOW"
    elif "reach_error" in property_text:
        encoding = "VERIFIERERROR"
    elif "F end" in property_text:
        encoding = "REACHASTERMINATION"
    else:
        raise ValueError(f"Unknown property in given property file {property_}")
    return encoding


def extract_type(witness):
    with open(witness, "r") as f:
        # Match GraphML witness
        witness_text = f.read()
        match = re.search(
            r'<data\s+key="witness-type">([a-zA-Z_]*)</data>',
            witness_text,
        )
        if match:
            return match.group(1)
        else:
            # Match YAML witness
            # We interpret an empty YAML witness as a correctness witness
            if len(witness_text) == 0 or \
                    '- entry_type: "invariant_set"' in witness_text:
                return "correctness_witness"
            elif '- entry_type: "violation_sequence"' in witness_text:
                return "violation_witness"

            return None


def extract_architecture(witness):
    with open(witness, "r") as f:
        witness_text = f.read()
        match = re.search(
            r'<data\s+key="architecture">([0-9][0-9])bit</data>',
            witness_text,
        )
        if match:
            return match.group(1)
        else:
            match = re.search(
                'data_model: ".?LP([0-9][0-9])"',
                witness_text,
            )
            if match:
                return match.group(1)
            elif len(witness_text) == 0:
                # We default to 32 bit for empty witnesses
                return "32"
            else:
                return None


def main(argv):
    args = get_argument_parser().parse_args(argv[1:])
    log = get_logger("METAVAL", "  %(name)s: %(message)s")
    if args.witnessType and not check_witness_type_matches(
            args.witness, args.witnessType
    ):
        log.info("witness type does not match!")
        return 1
    log.info(f"verifier used in MetaVal is {args.verifier}")
    log.info(f"witness is {args.witness}")
    if args.additionalPATH:
        log.info(f"additional path is {additionalPATH}")
        os.environ["PATH"] = f"{os.environ['PATH']}:{args.additionalPATH}"
    log.info(f"property file is specified as {args.property}")
    property_option = resolve_property(args.property)
    log.info(f"Task name: {args.taskname}")
    log.info(f"Wrapped args: {args.wrapped_args}")

    log.info("generating residual program from task and witness")
    TRANSFORMERCMD = [
        resource("CPAchecker-frontend/scripts/cpa.sh"),
        "-benchmark",
        "-heap",
        "10G",
        "-setprop",
        "cpa.arg.CTranslation.file=ARG.c",
        "-witness2reach",
        "-spec",
        args.witness,
        "-spec",
        args.property,
        args.taskname,
        "-setprop",
        "witness.useInvariantsAsAssumptions=true",
        "-setprop",
        "witness.checkInvariantViolations=true",
        "-setprop",
        "cpa.arg.export.code.header=false",
        "-setprop",
        "witness.strictChecking=false",
        "-setprop",
        "witness.checkProgramHash=false",
        "-setprop",
        f"cpa.arg.export.code.handleTargetStates={property_option}",
        "-outputpath",
        resource("./output"),
    ]
    arch = extract_architecture(args.witness)
    if not arch:
        log.error(
            f"Architecture could not be determined from the witness {args.witness}"
        )
        return 1
    TRANSFORMERCMD += [f"-{arch}"]
    if os.path.isfile(resource("output/ARG.c")):
        log.error(
            "transformed program already exists at location %s!"
            % resource("output/ARG.c")
        )
        return 1

    # FRONTEND
    log.info(f"Command for transformer: {TRANSFORMERCMD}")
    sys.stdout.flush()
    process = subprocess.Popen(  # noqa S603: we control cmd completely
        TRANSFORMERCMD, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    transformerlog = get_logger("FRONTEND", "  %(name)-5s: %(message)s")
    for bline in iter(process.stdout.readline, None):
        if not bline:
            break
        line = bline.decode("utf-8")
        transformerlog.info(line.strip("\n"))
    process.communicate()
    log.info(f"Frontend terminated with exit code: {process.returncode}")
    if process.returncode != 0:
        log.error("Frontend terminated with non-zero exit code => aborting!")
        return 1

    # BACKEND
    cmd = args.wrapped_args
    if args.verifier == "UAutomizer-linux":
        for i in range(len(cmd)):
            if os.path.isfile(cmd[i]):
                cmd[i] = os.path.abspath(cmd[i])
        tooldir = resource("./UAutomizer-linux")
        os.chdir(tooldir)
    log.info(f"Executing backend verifier {args.verifier}")
    process = subprocess.Popen(  # noqa S603: we control cmd completely
        args.wrapped_args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT
    )
    backendlog = get_logger("", "%(message)s")
    for bline in iter(process.stdout.readline, None):
        if not bline:
            break
        line = bline.decode("utf-8")
        backendlog.info(line.strip("\n"))
    process.communicate()
    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
