#! /bin/bash

# This file is part of MetaVal - a meta validator:
# https://gitlab.com/sosy-lab/software/metaval
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# This script requires a parameter --spec: path to a specification file.
# It will return a verifier to be used as a backend in MetaVal.

set -e

if [ "$1" == "--version" ]
  then
  echo "MetaVal algorithm selector version 0.1.0 (based on cpa-seq, uautomizer, symbiotic)"
  exit 0
fi

if [ "$1" != "--spec" ]
  then
  echo "expected --spec as first option, but got $1"
  exit 1
fi

shift
spec=$1
shift

filename=$(basename "$spec" ".prp")

if [ "$filename" == "unreach-call" ]
  then
  echo "cpa-seq" 
  exit 1
fi

if [ "$filename" == "no-overflow" ]
  then
  echo "uautomizer" 
  exit 1
fi

if [ "$filename" == "termination" ]
  then
  echo "uautomizer" 
  exit 1
fi

if [ "$filename" == "valid-memsafety" ]
  then
  echo "symbiotic" 
  exit 1
fi

# cpachecker in default case.
echo "cpa-seq" 
