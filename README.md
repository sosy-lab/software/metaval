# MetaVal

## Witness Validation via Verification

MetaVal takes as input a program, a specification, and a witness, and reduces the task of witness-based result validation to an ordinary verification task.
For the reduction, CPAchecker is used as program transformer.

As verification backend, any standard verifier can be used as long as it understands the specification format of SV-COMP.

## Tutorial

To get started with using MetaVal, we recommend to step through the [tutorial/](tutorial/)
after you finished the installation.

## Installation

MetaVal can be installed on a machine with the below properties,
or executed within a readily prepared virtual machine.

### Dependencies

MetaVal requires a machine with:
- Linux
- Python 3.6 or later
- Java 8 and Java 11 (for executing Java-based verifiers)

For reliable benchmarking, MetaVal is compatible with BenchExec.
For that you will need:
- [BenchExec](https://github.com/sosy-lab/benchexec) 2.5.1 or later

Please make sure that namespaces and cgroups are configured as described in the 
BenchExec [documentation](https://github.com/sosy-lab/benchexec/blob/master/doc/INSTALL.md).
Debian packages for benchexec can be found at https://github.com/sosy-lab/benchexec/releases,
with which most of the configuration is done automatically.

### Directory Structure

The MetaVal directory is structured as follows:

```
    .
    |-- bundle.sh                 # script to put together all components used in our examples
    |-- LICENSE                   # Apache 2.0 license file
    |-- metaval.sh                # script to start the validation process
    |-- README.md                 # Markdown version of this file
    |-- README.html               # HTML version of this file
    |-- tutorial                  # optional folder with execution examples
        |-- README.md             # explanation of how to execute MetaVal for different backend verifiers
        |-- README.html           # HTML version of the explanation
        |-- screenshots           # images used in the tutorial's explanation
        |-- witnesses             # files used in the examples, taken from SV-COMP'2020
        |-- programs              # programs used in the examples, taken from sv-benchmarks(svcomp20 tag)
        |-- tests_ci.yml          # benchmark definition for execution with BenchExec, all examples
        |-- tests_correctness.yml # benchmark definition for execution with BenchExec, only correctness
        |-- tests_violation.yml   # benchmark definition for execution with BenchExec, only violation
        |-- smoke_tests.sh        # for executing MetaVal directly on the examples
```

In addition, there are folders at the top level for each verifier that is used as backend.

### Virtual Machine

We have prepared a virtual machine (VM) with the required dependencies to ease the effort
to execute the examples, which is available at Zenodo
([DOI: 10.5281/zenodo.3630165](https://doi.org/10.5281/zenodo.3630165)).
Login credentials are: `user: cav20` and `password: cav20`.

The VM is configured to use 2 CPU cores and 10 GB of RAM.
Both can be lowered in case the host system does not provide these requirements.
The memory was chosen to be 10 GB in order to guarantee
that at least 7 GB are available to MetaVal in order to be consistent with the resource
limits that seem to be standard in the community.

The VM was created with VirtualBox in version 5.2.34 and
tested on a host machine with an i5-7440HQ CPU and 31 GB of RAM;
the CPU had a base frequency of 2.80 GHz and a turbo boost frequency of 3.80 GHz.
Both host and guest system used Ubuntu 18.04 as operating system.

The home directory contains a version of Metaval in ~/cav20/metaval,
where the README.md with instructions on how to use the tool can be found.
The folder `~/cav20/metaval/tutorial/` contains some example C programs, specifications, and witnesses,
and some further step-by-step instructions on how to execute MetaVal (cf. `~cav20/metaval/tutorial/README.md`).

# Usage

## Execute Using BenchExec

MetaVal is most conveniently called via BenchExec.
The benchmark definition needs to specify the following options:

    --metavalVerifierBackend (CPAchecker|symbiotic|ultimateautomizer|esbmc|yogar-cbmc)
    --metavalWitness <pathtowitness>

For some tools, a additional prefix to the PATH variable needs to be added (e.g. for Ultimate Automizer which still needs Java 8).
In those cases, the following option can be used:

    --metavalAdditionalPATH <additionalpath>

There is also an option to make sure MetVal is only validating correctness witnesses or onlly validating violation witnesses:
    --metavalWitnessType (correctness_witness|violation_witness)

These are then filtered by BenchExec's tool-info module and not actually passed to metaval.sh.

Example snippet from
[MetaVal's task definition](https://github.com/sosy-lab/sv-comp/blob/svcomp20/benchmark-defs/metaval-validate-violation-witnesses.xml)
in SV-COMP 2020:

    <benchmark tool="metaval" timelimit="90 s" hardtimelimit="96 s" memlimit="7 GB" cpuCores="2">
    ...
    <option name="--metavalWitness">../../results-verified/LOGDIR/${rundefinition_name}.${taskdef_name}/witness.graphml</option>
    <option name="--metavalVerifierBackend">cpachecker</option>

## Supported Verification Backends

The currently supported verification backends are:

- cpachecker-metaval: use the CPAchecker version from SV-COMP'2020 that is also used as program transformer
- cpachecker: use the CPAchecker version from the submission CPA-Seq to SV-COMP'2019
- symbiotic: use the Symbiotic version from the submission to SV-COMP'2019
- esbmc: use the ESBMC version from the submission to SV-COMP'2019
- yogar-cbmc: use the Yogar-CBMC version from the submission to SV-COMP'2019
- ultimateautomizer: use the Ultimate version from the submission UAutomizer to SV-COMP'2019

For extending MetaVal with new backends, all that needs to be done is placing the new tool's folder at the top level into the metaval directory.
If MetaVal is executed via BenchExec, then the list in the tool-info module needs be updated to also support the new tool.
We plan to remove this requirement in the future, such that the tool-info module delegates the discovery of new backends to MetaVal.

## Execute metaval.sh Directly

To call MetaVal directly, the following syntax can be used:

    metaval.sh --witness path/to/witness --verifier nameOfVerifier -- $BACKENDVERIFIERCMD

$BACKENDVERIFIERCMD is the command with which the backend verifier would normally be called, assuming its directory as working directory.
The file `../output/ARG.c` has to be used instead of the original program from the verification task, since MetaVal will create the reduced program there.

Example for using CPAchecker as verification backend:

    ./metaval.sh --verifier 'CPAchecker-1.7-svn 29852-unix' --witness witness.graphml \
    ../sv-benchmarks/c/bitvector/jain_1-1.c -- scripts/cpa.sh -predicateAnalysis-PredAbsRefiner-ABEl \
    -setprop cpa.predicate.memoryAllocationsAlwaysSucceed=true -timelimit 90s -stats \
    -spec ../../sv-benchmarks/c/properties/unreach-call.prp ../output/ARG.c

## References

- [MetaVal: Witness Validation via Verification](https://link.springer.com/content/pdf/10.1007/978-3-030-53291-8_10.pdf), by Dirk Beyer and Martin Spiessl. Proc. CAV. Springer (2020). [doi:10.1007/978-3-030-53291-8_10](https://doi.org/10.1007/978-3-030-53291-8_10)

